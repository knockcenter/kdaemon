"""
# -*- coding: utf-8 -*-
# ===============================================================================
#
# Copyright (C) 2013/2017 Laurent Labatut / Laurent Champagnac
#
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# ===============================================================================
"""
import glob
import logging
import os
import platform
import shutil
import sys
import ujson
import unittest
from datetime import datetime
from os.path import dirname, abspath
from unittest.util import safe_repr

import psutil
from dns.resolver import Resolver
from nose.plugins.attrib import attr
from pythonsol.FileUtility import FileUtility
from pythonsol.SolBase import SolBase
from pythonsol.meter.MeterManager import MeterManager

from knockdaemon.Api.ButcherTools import ButcherTools
from knockdaemon.Core.KnockHelpers import KnockHelpers
from knockdaemon.Core.KnockManager import KnockManager
from knockdaemon.Core.KnockStat import KnockStat
from knockdaemon.Platform.PTools import PTools
from knockdaemon.Probes.Apache.ApacheStat import ApacheStat
from knockdaemon.Probes.Inventory.Inventory import Inventory
from knockdaemon.Probes.MemCached.MemCachedStat import MemCachedStat
from knockdaemon.Probes.Mongodb.MongoDbStat import MongoDbStat
from knockdaemon.Probes.Mysql.Mysql import Mysql
from knockdaemon.Probes.Nginx.NGinxStat import NginxStat
from knockdaemon.Probes.Os.CheckDns import CheckDns
from knockdaemon.Probes.Os.CheckProcess import CheckProcess
from knockdaemon.Probes.Os.DiskSpace import DiskSpace
from knockdaemon.Probes.Os.HddStatus import HddStatus
from knockdaemon.Probes.Os.IpmiLog import IpmiLog
from knockdaemon.Probes.Os.IpvsAdm import IpvsAdm
from knockdaemon.Probes.Os.Load import Load
from knockdaemon.Probes.Os.Memory import Memory
from knockdaemon.Probes.Os.NetStat import Netstat
from knockdaemon.Probes.Os.Network import Network
from knockdaemon.Probes.Os.ProcNum import NumberOfProcesses
from knockdaemon.Probes.Os.TimeDiff import TimeDiff
from knockdaemon.Probes.Os.UpTime import Uptime
from knockdaemon.Probes.PhpFpm.PhpFpmStat import PhpFpmStat
from knockdaemon.Probes.Redis.RedisStat import RedisStat
from knockdaemon.Probes.Uwsgi.UwsgiStat import UwsgiStat
from knockdaemon.Probes.Varnish.VarnishStat import VarnishStat

SolBase.voodoo_init()
logger = logging.getLogger(__name__)


class TestProbesDirect(unittest.TestCase):
    """
    Test description
    """

    def setUp(self):
        """
        Setup (called before each test)
        """

        os.environ.setdefault("KNOCK_UNITTEST", "yes")

        self.current_dir = dirname(abspath(__file__)) + SolBase.get_pathseparator()
        self.config_file = \
            self.current_dir + "conf" + SolBase.get_pathseparator() \
            + "realall" + SolBase.get_pathseparator() + "knockdaemon.ini"

        # Config files
        for f in [
            "k.CheckProcess.json",
            "k.CheckDns.json",
            "knockdaemon.ini",
            SolBase.get_pathseparator().join(["conf.d", "10_auth.ini"])
        ]:
            src = self.current_dir + "conf" + SolBase.get_pathseparator() + "realall" + SolBase.get_pathseparator() + f
            dst = PTools.get_tmp_dir() + SolBase.get_pathseparator() + f

            dir_name = dirname(dst)
            if not FileUtility.is_dir_exist(dir_name):
                os.makedirs(dir_name)
            shutil.copyfile(src, dst)

            # Load
            buf = FileUtility.file_to_textbuffer(dst, "utf8")

            # Replace
            buf = buf.replace("/tmp", PTools.get_tmp_dir())

            # Write
            FileUtility.append_text_tofile(dst, buf, "utf8", overwrite=True)

        # Overwrite
        self.config_file = PTools.get_tmp_dir() + SolBase.get_pathseparator() + "knockdaemon.ini"

        # Reset meter
        MeterManager._hash_meter = dict()

        # Allocate, do not start
        self.k = KnockManager(self.config_file, auto_start=False)

        # Debug stat on exit ?
        self.debug_stat = False
        self.optional_key = list()

        # Overide parameter in probe to mock
        self.conf_probe_override = dict()

        # If windows, perform a WMI initial refresh to get datas
        if PTools.get_distribution_type() == "windows":
            from knockdaemon.Windows.Wmi.Wmi import Wmi
            Wmi._wmi_fetch_all()
            Wmi._flush_props(Wmi._WMI_DICT, Wmi._WMI_DICT_PROPS)

    def tearDown(self):
        """
        Setup (called after each test)
        """

        if self.debug_stat:
            ks = MeterManager.get(KnockStat)
            for k, v in ks.to_dict().iteritems():
                logger.info("stat, %s => %s", k, v)

    def _exec_helper(self, exec_class):
        """
        Exec helper
        """

        # Reset
        self.k._superv_notify_value_list = list()

        count = 0
        for p in self.k._probe_list:
            if isinstance(p, exec_class):
                for k, v in self.conf_probe_override.iteritems():
                    setattr(p, k, v)
                p.execute()
                count += 1
        self.assertGreater(count, 0)
        logger.info("Exec ok")

        for tu in self.k._superv_notify_value_list:
            logger.info("Having tu=%s", tu)

    def _expect_value(self, key, value, operator, cast_to_float=False):
        """
        Expect key to have value
        :param key str
        :type key str
        :param value str, int, float, None
        :type value str, int, float, None
        :param operator: str (eq, gte, lte, exists)
        :type operator str
        :param cast_to_float: bool
        :type cast_to_float bool
        """

        # LLA fix
        if isinstance(value, dict):
            value = ujson.dumps(value)

        hit = 0
        count = 0
        vlist = list()
        for tu in self.k._superv_notify_value_list:
            k = tu[0]
            v = tu[2]
            if cast_to_float:
                try:
                    v = float(v)
                except ValueError:
                    v = None

            if k == key:
                hit += 1
                vlist.append(v)
                if operator == "eq":
                    if v == value:
                        count += 1
                elif operator == "gte":
                    v = float(v)
                    value = float(value)
                    if v >= value:
                        count += 1
                elif operator == "lte":
                    v = float(v)
                    value = float(value)
                    if v <= value:
                        count += 1
                elif operator == "exists":
                    count += 1

        if count == 0:
            self.assertEqual(True, False, msg="Key/Value not found, hit={0}, count={1}, ope={2}, {3}={4}, vlist={5}".format(hit, count, operator, key, value, vlist))
            # raise Exception("Key/Value not found, ope={0}, {1}={2}".format(operator, key, value))
        self.assertEqual(True, True, msg='%s %s %s' % (key, value, operator))

    @unittest.skipIf(NginxStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % NginxStat())
    def test_NginxStat(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(NginxStat)

        # Validate results - disco
        local_list = list()
        nginx_instance = dict()
        nginx_instance['{#ID}'] = "default"
        local_list.append(nginx_instance)
        self._expect_value("k.nginx.discovery", dict(data=local_list), "eq")

        # Validate results - data
        self._expect_value("k.nginx.started[default]", 1, "eq")

        self._expect_value("k.nginx.reading[default]", 0, "gte")
        self._expect_value("k.nginx.writing[default]", 0, "gte")

        self._expect_value("k.nginx.connections[default]", 0, "gte")

        self._expect_value("k.nginx.waiting[default]", 0, "gte")
        self._expect_value("k.nginx.requests[default]", 1, "gte")
        self._expect_value("k.nginx.accepted[default]", 1, "gte")

    @unittest.skipIf(CheckDns().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % CheckDns())
    def test_CheckDns(self):
        """
        Test
        """

        # Exec it

        self._exec_helper(CheckDns)
        ns = Resolver().nameservers[0]
        # Validate results - disco
        local_list = list()
        dns_instance = dict()
        dns_instance['{#HOST}'] = "knock.center"
        dns_instance['{#SERVER}'] = ns
        local_list.append(dns_instance)
        self._expect_value("k.dns.discovery", dict(data=local_list), "eq")

        # Validate results - data
        self._expect_value('k.dns.resolv[knock.center,%s]' % ns, '198.27.81.204', "eq")
        self._expect_value('k.dns.time[knock.center,194.98.65.65]', 0, "gte")

    @unittest.skipIf(TimeDiff().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % TimeDiff())
    def test_TimeDiff(self):
        """
        Test
        """

        # Exec it

        self._exec_helper(TimeDiff)

        self._expect_value("k.os.timediff", 10, "lte", cast_to_float=True)

    @unittest.skipIf(Inventory().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % Inventory())
    def test_Inventory(self):
        """
        Test
        """

        # Requires SUDO :
        # user ALL=(ALL:ALL) NOPASSWD: /usr/sbin/dmidecode

        # Prepare
        (sysname, nodename, kernel, version, machine) = os.uname()
        (distribution, dversion, _) = platform.linux_distribution()

        # Exec it
        self._exec_helper(Inventory)

        # Check
        self._expect_value("k.inventory.os", "%s %s %s" % (sysname, distribution, dversion), "eq", cast_to_float=False)
        self._expect_value("k.inventory.kernel", kernel, "eq")
        self._expect_value("k.inventory.name", nodename, "eq")

        # Run dmidecode
        ec, so, si = ButcherTools.invoke("sudo dmidecode")
        if ec == 0 and so.find("sorry") == -1:
            logger.info("Assuming dmidecode OK")
            self._expect_value("k.inventory.vendor", None, "exists")
            self._expect_value("k.inventory.mem", None, "exists")
            self._expect_value("k.inventory.system", None, "exists")
            self._expect_value("k.inventory.chassis", None, "exists")
            self._expect_value("k.inventory.serial", None, "exists")
            self._expect_value("k.inventory.cpu", None, "exists")
        else:
            logger.info("Assuming dmidecode KO")

    @unittest.skipIf(HddStatus().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % HddStatus())
    def test_HddStatus(self):
        """
        Test
        """

        # Requires SUDO :
        # user ALL=(ALL:ALL) NOPASSWD: /usr/sbin/smartctl

        # Exec it
        self._exec_helper(HddStatus)

        # CANNOT VALIDATE ON vm, requires a PHYSICAL server (need /dev/sd*)
        hds = glob.glob('/dev/sd[a-z]')
        hds.extend(glob.glob('/dev/sd[a-z][a-z]'))
        if len(hds) == 0:
            logger.info("Assuming VM, lightweight checks")
            self._expect_value("k.hard.hd.status[ALL]", "OK", "eq")
            self._expect_value("k.hard.hd.user_capacity[ALL]", "ALL", "eq")
            self._expect_value("k.hard.hd.reallocated_sector_ct[ALL]", 0, "eq")
            self._expect_value("k.hard.hd.user_capacity[ALL]", "ALL", "eq")
            self._expect_value("k.hard.hd.serial_number[ALL]", "ALL", "eq")
            self._expect_value("k.hard.hd.model_family[ALL]", "ALL", "eq")
            self._expect_value("k.hard.hd.total_lbas_written[ALL]", 0, "eq")
            self._expect_value("k.hard.hd.health[ALL]", "KNOCKOK", "eq")
            self._expect_value("k.hard.hd.device_model[ALL]", "ALL", "eq")

            self._expect_value("k.hard.hd.discovery", None, "exists")
        else:
            # Try invoke on first sdX
            ec, so, se = ButcherTools.invoke("smartctl -q errorsonly -H -l selftest -b " + hds[0], timeout_ms=120000)
            logger.info("Got ec=%s, so=%s, se=%s", ec, so, se)
            if ec == 0:
                logger.info("Assuming PHYSICAL, heavy checks")
                self.assertFalse("Physical server heavy checks NOT implemented")
                # TODO : Implement unittest on a physical server please
            else:
                logger.info("smartctl invoke failed, bypassing checks")

    @unittest.skipIf(Load().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % Load())
    def test_Load(self):
        """
        Test
        """

        # ----------------------------
        # VALIDATE TRICK
        # ----------------------------

        # Example :
        # sec 0         50% cpu                         => server receive 50       = 50 (no previous value, we store it raw)
        # sec 60        50% cpu,    60s * 50% = 30      => server receive 50 + 30  = 80      => server delta = 30        => for 60 sec : 30 / 60 = 50%
        # sec 120       50% cpu,    60s * 50% = 30      => server receive 80 + 30  = 110     => server delta = 30        => for 60 sec : 30 / 60 = 50%
        # sec 180       100% cpu,   60s * 100% = 30     => server receive 110 + 60 = 170     => server delta = 60        => for 60 sec : 60 / 60 = 100%

        self.assertEqual(KnockHelpers.trick_instant_value_to_cumulative(None, 50.0, 0.0), 50.0)
        self.assertEqual(KnockHelpers.trick_instant_value_to_cumulative(50.0, 50.0, 60.0), 80.0)
        self.assertEqual(KnockHelpers.trick_instant_value_to_cumulative(80.0, 50.0, 60.0), 110.0)
        self.assertEqual(KnockHelpers.trick_instant_value_to_cumulative(110.0, 100.0, 60.0), 170.0)

        v_max = sys.float_info.max
        v_prev = v_max - 100.0
        v_cur = 202.0
        self.assertEqual(KnockHelpers.trick_instant_value_to_cumulative(v_prev, v_cur, 100.0), 202.0)

        # ----------------------------
        # VALIDATE WINDOWS LOAD API
        # ----------------------------

        # Keep current ms
        ms_cur = 1489440100000

        # Validate windows queue length processing
        ar = list()
        for min_back in range(20, 0, -1):
            ms_back = min_back * 60 * 1000
            ms_set = ms_cur - ms_back
            ar.append({"ms": ms_set, "q": min_back * 10})
        logger.info("ar_in=%s", ar)
        len_in = len(ar)

        # Get load
        load1, load5, load15 = Load.process_ar_queue(ar, ms_cur)
        logger.info("ar_out=%s", ar)
        len_out = len(ar)

        # We must have evictions
        self.assertLess(len_out, len_in)
        for d in ar:
            # All items must be less than 15 minutes
            self.assertLessEqual(SolBase.msdiff(d["ms"], ms_cur), 15 * 60 * 1000)

        # Check load
        self.assertEqual(load1, 10.0)
        self.assertEqual(load5, 10.0)
        self.assertEqual(load15, 80.0)

        # ---------------------------
        # Parse datetime (windows)
        # ---------------------------
        for ar in [
            # Offset -420 (ie -7H based on utc : Hour=4 => +7 : Hour=11 in utc)
            ["20170312044209.003363-420", datetime(year=2017, month=3, day=12, hour=11, minute=42, second=9, microsecond=3363)],
        ]:
            # GO
            s_dt = ar[0]
            c_dt = ar[1]
            d_dt = Load.parse_time(s_dt)
            logger.info("Got c_dt=%s", c_dt)
            logger.info("Got d_dt=%s", d_dt)
            self.assertEqual(c_dt, d_dt)

        # ----------------------------
        # Exec it
        # ----------------------------
        for _ in range(2):
            self._exec_helper(Load)
            self._expect_value("k.os.cpu.load[percpu,avg1]", None, "exists")
            self._expect_value("k.os.cpu.load[percpu,avg5]", None, "exists")
            self._expect_value("k.os.cpu.load[percpu,avg15]", None, "exists")
            self._expect_value("k.os.cpu.core", 1, "gte")
            self._expect_value("k.os.cpu.util[,softirq]", None, "exists")
            self._expect_value("k.os.cpu.util[,iowait]", None, "exists")
            self._expect_value("k.os.cpu.util[,system]", None, "exists")
            self._expect_value("k.os.cpu.util[,idle]", None, "exists")
            self._expect_value("k.os.cpu.util[,user]", None, "exists")
            self._expect_value("k.os.cpu.util[,interrupt]", None, "exists")
            self._expect_value("k.os.cpu.util[,steal]", None, "exists")
            self._expect_value("k.os.cpu.util[,nice]", None, "exists")
            self._expect_value("k.os.cpu.switches", None, "exists")
            self._expect_value("k.os.cpu.intr", None, "exists")
            self._expect_value("k.os.boottime", None, "exists")
            self._expect_value("k.hard.cpu.count[,,run]", None, "exists")
            self._expect_value("k.os.hostname", None, "exists")
            self._expect_value("k.os.localtime", None, "exists")
            self._expect_value("k.os.maxfiles", None, "exists")
            self._expect_value("k.os.maxproc", None, "exists")
            self._expect_value("k.os.users.num", 0, "gte")

    @unittest.skipIf(RedisStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % RedisStat())
    def test_RedisStat(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(RedisStat)

        # Validate KEYS
        for cur_port in ["6379", "ALL"]:
            for _, knock_type, knock_key, _ in RedisStat.KEYS:
                if knock_type == "int":
                    self._expect_value(knock_key + "[" + cur_port + "]", -1, "gte")
                elif knock_type == "float":
                    self._expect_value(knock_key + "[" + cur_port + "]", 0.0, "gte")
                elif knock_type == "str":
                    self._expect_value(knock_key + "[" + cur_port + "]", 0, "exists")

        self._expect_value("k.redis.discovery", None, "exists")

    @unittest.skipIf(MemCachedStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % MemCachedStat())
    @attr('prov')
    def test_MemCachedStat(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(MemCachedStat)

        # Validate KEYS
        for cur_connect_to in ["11211", "ALL"]:
            for _, knock_type, knock_key, _ in MemCachedStat.KEYS:
                if knock_type == "int":
                    self._expect_value(knock_key + "[" + cur_connect_to + "]", -1, "gte")
                elif knock_type == "float":
                    self._expect_value(knock_key + "[" + cur_connect_to + "]", 0.0, "gte")
                elif knock_type == "str":
                    self._expect_value(knock_key + "[" + cur_connect_to + "]", 0, "exists")

        self._expect_value("k.memcached.discovery", None, "exists")

    @unittest.skipIf(DiskSpace().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % DiskSpace())
    def test_DiskSpace(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(DiskSpace)
        SolBase.sleep(1000)
        self._exec_helper(DiskSpace)
        if PTools.get_distribution_type() == "windows":
            ar = ["C:", "ALL"]
        else:
            ar = ["/", "ALL"]

        for cur_p in ar:
            self._expect_value("k.vfs.fs.size[" + cur_p + ",free]", 1, "gte")

            self._expect_value("k.vfs.fs.size[" + cur_p + ",pfree]", 0.0, "gte")
            self._expect_value("k.vfs.fs.size[" + cur_p + ",pfree]", 101.0, "lte")

            self._expect_value("k.vfs.fs.inode[" + cur_p + ",pfree]", 0.0, "gte")
            self._expect_value("k.vfs.fs.inode[" + cur_p + ",pfree]", 101.0, "lte")

            self._expect_value("k.vfs.fs.size[" + cur_p + ",total]", 1, "gte")
            self._expect_value("k.vfs.fs.size[" + cur_p + ",used]", 1, "gte")

            self._expect_value("k.vfs.dev.read.totalcount[" + cur_p + "]", 0, "gte")
            self._expect_value("k.vfs.dev.read.totalsectorcount[" + cur_p + "]", 0, "gte")
            self._expect_value("k.vfs.dev.read.totalbytes[" + cur_p + "]", 0, "gte")
            self._expect_value("k.vfs.dev.read.totalms[" + cur_p + "]", 0, "gte")
            self._expect_value("k.vfs.dev.write.totalcount[" + cur_p + "]", 0, "gte")
            self._expect_value("k.vfs.dev.write.totalsectorcount[" + cur_p + "]", 0, "gte")
            self._expect_value("k.vfs.dev.write.totalbytes[" + cur_p + "]", 0, "gte")
            self._expect_value("k.vfs.dev.write.totalms[" + cur_p + "]", 0, "gte")
            self._expect_value("k.vfs.dev.io.currentcount[" + cur_p + "]", 0, "gte")
            self._expect_value("k.vfs.dev.io.totalms[" + cur_p + "]", 0, "gte")

    @unittest.skipIf(IpmiLog().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % IpmiLog())
    def test_IpmiLog(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(IpmiLog)

        # TODO : check IpmiLog (need install)

    @unittest.skipIf(IpvsAdm().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % IpvsAdm())
    def test_IpvsAdm(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(IpvsAdm)

    # TODO : check IpmiLog (need install)

    @unittest.skipIf(Memory().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % Memory())
    def test_Memory(self):
        """
        Test
        """

        # Exec it
        for _ in range(2):
            self._exec_helper(Memory)

            self._expect_value("k.os.memory.size[available]", 0, "gte")
            self._expect_value("k.os.swap.size[,free]", 0, "gte")

            self._expect_value("k.os.swap.size[,pfree]", 0.0, "gte")
            self._expect_value("k.os.swap.size[,pfree]", 101.0, "lte")

            self._expect_value("k.os.memory.size[total]", 0, "gte")
            self._expect_value("k.os.swap.size[,total]", 0, "gte")
            self._expect_value("k.os.memory.size[,buffers]", 0, "gte")
            self._expect_value("k.os.memory.size[,cached]", 0, "gte")
            self._expect_value("k.os.memory.size[,used]", 0, "gte")
            self._expect_value("k.os.swap.size[,used]", 0, "gte")

    @unittest.skipIf(Memory().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % Memory())
    def test_Memory_mock(self):
        """
        Test
        """

        # Exec it
        mem_file = os.path.join(dirname(abspath(__file__)), 'conf/mock_mem.txt')
        self.conf_probe_override['mem_info'] = mem_file
        self._exec_helper(Memory)

        self._expect_value("k.os.memory.size[available]", 0, "gte")
        self._expect_value("k.os.swap.size[,free]", 0, "gte")

        self._expect_value("k.os.swap.size[,pfree]", 0.0, "gte")
        self._expect_value("k.os.swap.size[,pfree]", 101.0, "lte")

        self._expect_value("k.os.memory.size[total]", 0, "gte")
        self._expect_value("k.os.swap.size[,total]", 0, "gte")
        self._expect_value("k.os.memory.size[,buffers]", 0, "gte")
        self._expect_value("k.os.memory.size[,cached]", 0, "gte")
        self._expect_value("k.os.memory.size[,used]", 0, "gte")
        self._expect_value("k.os.swap.size[,used]", 0, "gte")

    @unittest.skipIf(Netstat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % Netstat())
    def test_Netstat(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(Netstat)

        self._expect_value("k.net.netstat[SYN_SENT]", 0, "gte")
        self._expect_value("k.net.netstat[LISTEN]", 0, "gte")
        self._expect_value("k.net.netstat[TIME_WAIT]", 0, "gte")
        self._expect_value("k.net.netstat[SYN_RECV]", 0, "gte")
        self._expect_value("k.net.netstat[LAST_ACK]", 0, "gte")
        self._expect_value("k.net.netstat[CLOSE_WAIT]", 0, "gte")
        self._expect_value("k.net.netstat[CLOSED]", 0, "gte")
        self._expect_value("k.net.netstat[FIN_WAIT2]", 0, "gte")
        self._expect_value("k.net.netstat[FIN_WAIT1]", 0, "gte")
        self._expect_value("k.net.netstat[ESTABLISHED]", 0, "gte")
        self._expect_value("k.net.netstat[CLOSING]", 0, "gte")

    @unittest.skipIf(Network().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % Network())
    def test_Network(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(Network)

        if PTools.get_distribution_type() == "windows":
            ar = ("dynamic", "dynamic")
        else:
            ar = ("lo", "LoopBack")
        for cur_n, cur_type in [ar]:
            # --------------------
            # If dynamic, extract first stuff
            # --------------------
            if cur_n == "dynamic":
                # Windows, fetch first and extract
                for tu in self.k._superv_notify_value_list:
                    k = tu[0]
                    v = tu[2]
                    if k.startswith("k.net.if.type"):
                        cur_n = k.replace("k.net.if.type[", "").replace("]", "")
                        cur_type = v
                        logger.info("Got cur_n=%s, cur_type=%s", cur_n, cur_type)
                        break

            # --------------------
            # GO
            # --------------------
            self._expect_value("k.net.if.status[" + cur_n + ",status]", "ok", "eq")
            self._expect_value("k.eth.bytes.recv[" + cur_n + "]", 0, "gte")
            self._expect_value("k.eth.bytes.sent[" + cur_n + "]", 0, "gte")
            self._expect_value("k.net.if.status[" + cur_n + ",duplex]", "full", "eq")
            self._expect_value("k.net.if.status[" + cur_n + ",speed]", 100.0, "gte")
            self._expect_value("k.net.if.type[" + cur_n + "]", cur_type, "eq")
            self._expect_value("k.net.if.status[" + cur_n + ",mtu]", 0, "gte")
            self._expect_value("k.net.if.status[" + cur_n + ",address]", None, "exists")
            self._expect_value("k.net.if.status[" + cur_n + ",tx_queue_len]", 0, "gte")
            self._expect_value("k.eth.errors.recv[" + cur_n + "]", 0, "gte")
            self._expect_value("k.eth.errors.sent[" + cur_n + "]", 0, "gte")
            self._expect_value("k.eth.missederrors.recv[" + cur_n + "]", 0, "gte")
            self._expect_value("k.eth.packet.recv[" + cur_n + "]", 0, "gte")
            self._expect_value("k.eth.packet.sent[" + cur_n + "]", 0, "gte")
            self._expect_value("k.eth.packetdrop.recv[" + cur_n + "]", 0, "gte")
            self._expect_value("k.eth.packetdrop.sent[" + cur_n + "]", 0, "gte")
            self._expect_value("k.net.if.collisions[" + cur_n + "]", 0, "gte")

    @unittest.skipIf(NumberOfProcesses().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % NumberOfProcesses())
    def test_NumberOfProcesses(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(NumberOfProcesses)

        self._expect_value("k.hard.cpu.count[]", 1, "gte")

    @unittest.skipIf(Uptime().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % Uptime())
    def test_Uptime(self):
        """
        Test
        """

        # ---------------------------
        # Exec it
        # ---------------------------
        for _ in range(2):
            self._exec_helper(Uptime)

            self._expect_value("k.os.knock", 1, "gte")
            self._expect_value("k.os.uptime", 1, "gte")

    @unittest.skipIf(PhpFpmStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % PhpFpmStat())
    @attr('prov')
    def test_PhpFpmStat(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(PhpFpmStat)

        for _, knock_type, knock_key in PhpFpmStat.KEYS:
            for pool_id in ["[www]", "[ALL]"]:
                if knock_type == "int":
                    self._expect_value(knock_key + pool_id, 0, "gte")
                elif knock_type == "float":
                    self._expect_value(knock_key + pool_id, 0.0, "gte")
                elif knock_type == "str":
                    self._expect_value(knock_key + pool_id, 0, "exists")

        self._expect_value("k.phpfpm.discovery", None, "exists")

    @unittest.skipIf(ApacheStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % ApacheStat())
    @attr('prov')
    def test_Apache(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(ApacheStat)

        for _, knock_type, knock_key in ApacheStat.KEYS:
            if knock_type == "int":
                self._expect_value(knock_key + "[default]", 0, "gte")
            elif knock_type == "float":
                self._expect_value(knock_key + "[default]", 0.0, "gte")
            elif knock_type == "str":
                self._expect_value(knock_key + "[default]", 0, "exists")

        self._expect_value("k.apache.discovery", None, "exists")

    @unittest.skipIf(ApacheStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % ApacheStat())
    @attr('prov')
    def test_Apache_from_dict_1(self):
        """
        Test
        """
        d = {'k.apache.status.ms': 100, 'k.apache.sc.send_reply': 3, 'Uptime': 599634.0, 'IdleWorkers': 12.0, 'k.apache.sc.waiting_for_connection': 12, 'k.apache.sc.keepalive': 0,
             'k.apache.sc.dns_lookup': 0, 'Total Accesses': 11934709.0, 'k.apache.sc.closing': 1, 'Total kBytes': 33635364.0, 'BytesPerReq': 2885.92, 'k.apache.sc.reading_request': 0, 'CPULoad': 0.06,
             'BytesPerSec': 57439.4, 'k.apache.sc.gracefully': 0, 'k.apache.sc.starting_up': 0, 'ReqPerSec': 19.9, 'k.apache.sc.open': 240, 'k.apache.sc.idle': 0, 'k.apache.sc.logging': 0,
             'BusyWorkers': 4.0}
        ap = ApacheStat()
        ap.set_manager(self.k)
        ap.process_apache_dict(d, "default")

        # Log
        for tu in self.k._superv_notify_value_list:
            logger.info("Having tu=%s", tu)

        # Check
        for _, knock_type, knock_key in ApacheStat.KEYS:
            if knock_type == "int":
                self._expect_value(knock_key + "[default]", 0, "gte")
            elif knock_type == "float":
                self._expect_value(knock_key + "[default]", 0.0, "gte")
            elif knock_type == "str":
                self._expect_value(knock_key + "[default]", 0, "exists")

        # Discovery is fired outside this, do not check it here
        pass

    @unittest.skipIf(VarnishStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % VarnishStat())
    @attr('prov')
    def test_Varnish(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(VarnishStat)

        for _, knock_type, knock_key in VarnishStat.KEYS:
            if knock_type == "int":
                self._expect_value(knock_key + "[default]", 0, "gte")
            elif knock_type == "float":
                self._expect_value(knock_key + "[default]", 0.0, "gte")
            elif knock_type == "str":
                self._expect_value(knock_key + "[default]", 0, "exists")

        self._expect_value("k.varnish.discovery", None, "exists")

    @unittest.skipIf(VarnishStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % VarnishStat())
    @attr('prov')
    def test_Varnish_via_invoke_json(self):
        """
        Test
        """

        vp = VarnishStat()
        vp.set_manager(self.k)

        # ---------
        # JSON
        # ---------
        d_json = vp.try_load_json()
        vp.process_json(d_json, "default")
        # Log
        for tu in self.k._superv_notify_value_list:
            logger.info("Having tu=%s", tu)

        for _, knock_type, knock_key in VarnishStat.KEYS:
            if knock_type == "int":
                self._expect_value(knock_key + "[default]", 0, "gte")
            elif knock_type == "float":
                self._expect_value(knock_key + "[default]", 0.0, "gte")
            elif knock_type == "str":
                self._expect_value(knock_key + "[default]", 0, "exists")

        # Discovery is fired outside this, do not check it here
        pass

    @unittest.skipIf(VarnishStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % VarnishStat())
    @attr('prov')
    def test_Varnish_via_invoke_text(self):
        """
        Test
        """

        vp = VarnishStat()
        vp.set_manager(self.k)

        # ---------
        # JSON
        # ---------
        d_json = vp.try_load_text()
        vp.process_json(d_json, "default")
        # Log
        for tu in self.k._superv_notify_value_list:
            logger.info("Having tu=%s", tu)

        for _, knock_type, knock_key in VarnishStat.KEYS:
            if knock_type == "int":
                self._expect_value(knock_key + "[default]", 0, "gte")
            elif knock_type == "float":
                self._expect_value(knock_key + "[default]", 0.0, "gte")
            elif knock_type == "str":
                self._expect_value(knock_key + "[default]", 0, "exists")

        # Discovery is fired outside this, do not check it here
        pass

    @unittest.skipIf(UwsgiStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % UwsgiStat())
    @attr('prov')
    def test_UwsgiStat(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(UwsgiStat)

        for cur_p in [
            'z_frontends',
            'ALL'
        ]:
            for _, knock_type, knock_key in UwsgiStat.KEYS:
                if knock_type == "int":
                    self._expect_value(knock_key + "[" + cur_p + "]", 0, "gte")
                elif knock_type == "float":
                    self._expect_value(knock_key + "[" + cur_p + "]", 0.0, "gte")
                elif knock_type == "str":
                    self._expect_value(knock_key + "[" + cur_p + "]", 0, "exists")

            self._expect_value("k.uwsgi.discovery", None, "exists")

    @unittest.skipIf(CheckProcess().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % CheckProcess())
    def test_CheckProcess(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(CheckProcess)

        # Ar
        if PTools.get_distribution_type() == "windows":
            ar = ["win_idle_process"]
        else:
            ar = ["nginx"]

        # Using direct
        for cur_p in ar:
            self._expect_value("k.proc.pidfile[" + cur_p + "]", "ok", "eq")
            self._expect_value("k.proc.running[" + cur_p + "]", "ok", "eq")
            self._expect_value("k.proc.io.num_fds[" + cur_p + "]", 0, "gte")
            self._expect_value("k.proc.memory_used[" + cur_p + "]", 0, "gte")
            self._expect_value("k.proc.cpu_used[" + cur_p + "]", 0, "gte")
            self._expect_value("k.proc.nbprocess[" + cur_p + "]", 0, "gte")

            # Kernel too old may not support that (Couldn't find /proc/xxxx/io)
            # If OUR process has them, check them, otherwise discard
            self_pid = str(os.getpid())
            if FileUtility.is_file_exist("/proc/" + self_pid + "/io"):
                try:
                    p = psutil.Process(int(self_pid))
                    _ = p.io_counters()
                    # Working, check
                    self._expect_value("k.proc.io.read_bytes[" + cur_p + "]", 0, "gte")
                    self._expect_value("k.proc.io.write_bytes[" + cur_p + "]", 0, "gte")
                except Exception as e:
                    logger.warn("io_counters failed, bypassing checks, ex=%s", SolBase.extostr(e))

        # Using arrays
        # Ar
        if PTools.get_distribution_type() == "windows":
            ar = ["win_idle_process_array"]
        else:
            ar = ["nginx_array"]
        for cur_p in ar:
            self._expect_value("k.proc.pidfile[" + cur_p + "]", "ok", "eq")
            self._expect_value("k.proc.running[" + cur_p + "]", "ok", "eq")
            self._expect_value("k.proc.io.num_fds[" + cur_p + "]", 0, "gte")
            self._expect_value("k.proc.memory_used[" + cur_p + "]", 0, "gte")
            self._expect_value("k.proc.cpu_used[" + cur_p + "]", 0, "gte")
            self._expect_value("k.proc.nbprocess[" + cur_p + "]", 0, "gte")

            # Kernel too old may not support that (Couldn't find /proc/xxxx/io)
            # If OUR process has them, check them, otherwise discard
            self_pid = str(os.getpid())
            if FileUtility.is_file_exist("/proc/" + self_pid + "/io"):
                try:
                    p = psutil.Process(int(self_pid))
                    _ = p.io_counters()
                    # Working, check
                    self._expect_value("k.proc.io.read_bytes[" + cur_p + "]", 0, "gte")
                    self._expect_value("k.proc.io.write_bytes[" + cur_p + "]", 0, "gte")
                except Exception as e:
                    logger.warn("io_counters failed, bypassing checks, ex=%s", SolBase.extostr(e))

    @unittest.skipIf(Mysql().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % Mysql())
    @attr('prov')
    def test_Mysql(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(Mysql)

        for _, knock_type, knock_key in Mysql.KEYS:
            if knock_type == "int":
                if knock_key == "k.mysql.repli.cur.lag_sec":
                    self._expect_value(knock_key + "[default]", -2, "gte")
                else:
                    self._expect_value(knock_key + "[default]", 0, "gte")
            elif knock_type == "float":
                self._expect_value(knock_key + "[default]", 0.0, "gte")
            elif knock_type == "str":
                self._expect_value(knock_key + "[default]", 0, "exists")

        self._expect_value("k.mysql.discovery", None, "exists")

    @unittest.skipIf(MongoDbStat().is_supported_on_platform() is False, "Not support on current platform, probe=%s" % MongoDbStat())
    @unittest.skip("zzz")
    @attr('prov')
    def test_MongoDbStat(self):
        """
        Test
        """

        # Exec it
        self._exec_helper(MongoDbStat)

    def assertZTypeValue(self, cur_type, value, msg=None):
        """
        0 - numeric float;
        1 - character;
        2 - log;
        3 - numeric unsigned;
        4 - text.
        :param cur_type:
        :return:
        """
        human_type = {
            0: 'float',
            1: 'character len<256',
            2: 'log len<1024',
            3: 'numeric unsigned',
            4: 'text'
        }

        # float
        if cur_type == 0:
            try:
                value_float = float(value)
            except TypeError:
                value_float = None
            if not isinstance(value_float, float):
                msg = self._formatMessage(None, "%s %s is not a %s" % (msg, safe_repr(value), human_type[cur_type]))
                raise self.failureException(msg)

        elif cur_type == 1:
            if not (isinstance(value, basestring) and len(value) < 255):
                msg = self._formatMessage(None, "%s %s is not a %s" % (msg, safe_repr(value), human_type[cur_type]))
                raise self.failureException(msg)

        elif cur_type == 2:
            if not (isinstance(value, basestring) and len(value) < 1023):
                msg = self._formatMessage(None, "%s %s is not a %s" % (msg, safe_repr(value), human_type[cur_type]))
                raise self.failureException(msg)

        elif cur_type == 3:
            if not (isinstance(value, (int, long)) and value >= 0):
                msg = self._formatMessage(None, "%s %s is not a %s" % (msg, safe_repr(value), human_type[cur_type]))
                raise self.failureException(msg)

        elif cur_type == 4:
            if not isinstance(value, basestring):
                msg = self._formatMessage(None, "%s %s is not a %s" % (msg, safe_repr(value), human_type[cur_type]))
                raise self.failureException(msg)
        else:
            msg = self._formatMessage(None, "%s %s unknow, must be {0-4}" % (msg, safe_repr(cur_type)))
            raise self.failureException(msg)
